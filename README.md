# Final Project for Deep Learning Theory and Practice at the University of Strathclyde

## Paper titled "Comparing Time and Frequency Domain Autoencoders for Neural Audio Compression"

This repository hosts the notebook/Python code for the project and paper mentioned in the title. The main two notebook files are `AudioMNISTProjectNoPool.ipynb` (for the time-domain AE) and `AudioMNISTProjectSpec.ipynb` (for the frequency-domain AE). The DL library used was Pytorch Lightning.

![Paper screenshot](paper_screenshot.png)
